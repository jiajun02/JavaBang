<p align="center">
  <b><a href="#" target="_blank">Java Bang </a></b><br>
  <a href="#" target="_blank">
      <img src="https://static-public.chatopera.com/assets/images/44992890-38be0800-afcb-11e8-8fde-a5a671d29764.png" width="300">
  </a>
</p>

**目录(善用Ctrl+F)**

## :computer: 操作系统

- [计算机操作系统]
- **Linux**
    - **Linux基础**
        - [文件及目录管理](https://mp.weixin.qq.com/s/UBcgKr_GEjMzGw444oaNWA)
        - [磁盘管理](https://mp.weixin.qq.com/s/m9Dd5YlWiODdWeSWC1s2qw)
        - [进程管理](https://mp.weixin.qq.com/s/jSg25MpE_dgTvTslhuS3YQ)
        - [性能监控](https://mp.weixin.qq.com/s/pA58kay14LfsPSsSIQYH4w)
        - [pstack 跟踪进程栈](https://mp.weixin.qq.com/s/GdIBG1FA8ZnhHMG9mwsPFQ)
        - [sar 找出系统瓶颈的利器](https://mp.weixin.qq.com/s/85m4jPQLup-GeS2KIxhRog)
    - [ :fire: 查看更多...](https://gitee.com/iByteCoding/JavaBang/blob/master/javabang/%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F/Linux/README.md)
## :cloud: 网络 

- [计算机网络]
- [HTTP]
- [Socket]

## :art: 设计模式

- [设计模式]

## :floppy_disk: 数据库

- [数据库系统原理]
- **MySQL**
  - [MySQL在grant语句之后要跟着flush privileges吗？](https://mp.weixin.qq.com/s/Bs57Nb12w_YdTiBWd34Rkw)
  - [MySQL为什么还有kill不掉的语句？](https://mp.weixin.qq.com/s/4VhCmyO2EpHJZrECJp_dxA)
  - [查询请求增加时，如何做主从分离？](https://mp.weixin.qq.com/s/C9HQ15t8rsjsAlLJBAj5xg)
  - [MySQL性能优化一：多种优化 方式介绍](https://mp.weixin.qq.com/s/3qfkfeZOcERR8AB4rzqO8w)
  - [MySQL性能优化(二)：优化数据库的设计](https://mp.weixin.qq.com/s/xafmfOOT3sEuDftdrVvZ8A)
  - [MySQL性能优化(三)：索引](https://mp.weixin.qq.com/s/OxBNSepOs1qFtAwguF8dfw)
  - [MySQL性能优化(四)：分表](https://mp.weixin.qq.com/s/LOY4Wo1ueGAij4CgMuxebw)
  - [MySQL性能优化(五)：分区](https://mp.weixin.qq.com/s/2TPMowGzhhOxZgs_TriMpg)
  - [MySQL性能优化(六)：其他优化](https://mp.weixin.qq.com/s/_NcY_7k02lIm-n5sOn9p2w)
  - [MySQL索引的原理，B+树、聚集索引和二级索引的结构分析](https://mp.weixin.qq.com/s/rWpooijYm3HeQZw3ESZq6w)
  - [ :fire: 查看更多...](https://gitee.com/iByteCoding/JavaBang/tree/master/javabang/%E6%95%B0%E6%8D%AE%E5%BA%93)
- **Redis**
  - [宕机后，Redis如何实现快速恢复？](https://mp.weixin.qq.com/s/5GHiMu_KxmHW1sVsPsuXvw)
  - [Redis主从复制以及主从复制原理](https://mp.weixin.qq.com/s/0Y72MGcah8xcIJHU5f5NxA)
  - [用Redis构建缓存集群的最佳实践有哪些？](https://mp.weixin.qq.com/s/5eSAl8XpGgD-ZFs4ycdeXg)
  - [Redis数据增多了，是该加内存还是加实例？](https://mp.weixin.qq.com/s/oatPamGLW6ddRg2Ue5-vTA)
  - [ :fire: 查看更多...](https://gitee.com/iByteCoding/JavaBang/tree/master/javabang/%E6%95%B0%E6%8D%AE%E5%BA%93)

## :coffee: **Java**

- **Java 基础** [:fire: 查看更多...](https://gitee.com/iByteCoding/JavaBang/tree/master/javabang/Java/Java%20%E5%9F%BA%E7%A1%80)
    - [图解 Java 集合框架](https://blog.csdn.net/li1669852599/article/details/108138972)。
       - [ArrayList](https://blog.csdn.net/li1669852599/article/details/108131414)。
       - [LinkedList](https://t.1yb.co/3HxL)。
       - [Stack and Queue](https://t.1yb.co/3Luz)
       - [TreeSet and TreeMap](https://blog.csdn.net/li1669852599/article/details/108133485)
       - [HashSet and HashMap](https://t.1yb.co/3Lvz)
       - [LinkedHashSet and LinkedHashMap](https://blog.csdn.net/li1669852599/article/details/108138929)
       - [PriorityQueue](https://blog.csdn.net/li1669852599/article/details/108138942)
       - [WeakHashMap](https://blog.csdn.net/li1669852599/article/details/108138950)
- [Java 容器]
- **Java 并发**
    - [Semaphore：如何快速实现一个限流器？](https://t.1yb.co/466o)
- [Java 虚拟机]
- [Java I/O]
- Spring
    - [Spring 核心编程思想与实践](https://gitee.com/iByteCoding/thinking-in-spring)

## :bulb: 系统设计 

- [系统设计基础]
- **分布式**
- **集群**
    - [用Redis构建缓存集群的最佳实践有哪些？](https://mp.weixin.qq.com/s/5eSAl8XpGgD-ZFs4ycdeXg)
    - [哨兵机制：主库挂了，如何不间断服务？](https://mp.weixin.qq.com/s/3eqOZItXMsj0rt3GNF6HOA)
- [攻击技术]
- [缓存]
- **消息队列**
     - [Apache Kafka 真的只是消息引擎吗？](https://t.1yb.co/3TqJ)

## :wrench: 工具 

- **Git**
   - [从0到1实现前后端分离项目的gitlab-ci流程](https://mp.weixin.qq.com/s/pFLd7dVwgv-bq_cbKQN8aQ)
- **Docker**
   - [Java程序运行在Docker等容器环境有哪些新问题？](https://mp.weixin.qq.com/s/JiCT873FzI3ggdPjnpcI7g)
- [构建工具]
- [正则表达式]
- **开发工具**
    - [ideaIU-2018.3.3破解及安装](https://gitee.com/iByteCoding/JavaBang/blob/master/javabang/%E5%B7%A5%E5%85%B7/ideaIU-2018.3.3%E7%A0%B4%E8%A7%A3%E5%8F%8A%E5%AE%89%E8%A3%85.md)
    - [JetBrains 2020.2 版本 全家桶激活方式](https://gitee.com/iByteCoding/JavaBang/blob/master/javabang/%E5%B7%A5%E5%85%B7/JetBrains%202020.2%20%E7%89%88%E6%9C%AC%20%E5%85%A8%E5%AE%B6%E6%A1%B6%E6%BF%80%E6%B4%BB%E6%96%B9%E5%BC%8F.md)

## :watermelon: 编码实践 

- **代码可读性**
    - [Java 代码精简之道 (上)](https://mp.weixin.qq.com/s/o63PKK95KtVXWiU-th-tyw)
    - [Java 代码精简之道 (中)](https://mp.weixin.qq.com/s/YWdb2hMMaHX9I60NbSbZMg)
    - [Java 代码精简之道 (下)](https://mp.weixin.qq.com/s/0v4O3eclCzmcLPPI7uuOzQ)
- 代码风格规范
    - [阿里人-Java 开发手册 嵩山版](https://mp.weixin.qq.com/s/fAeIzAE0Op1OdM4Jdrhfyw)

## :100: 电子书
- [免费-电子书专栏](https://gitee.com/iByteCoding/JavaBang/blob/master/javabang/%E7%94%B5%E5%AD%90%E4%B9%A6/README.md)
- [小程序案例学习](https://mp.weixin.qq.com/s/9A4I23TLBzg8mCXRqvxp6A)

## :8ball: 志哥杂谈
- [除了摆地摊，你还可以选择东南亚跨境电商Shopee](https://mp.weixin.qq.com/s/TWkUSbjXW8aHhst2XEHv8g)
- [奈雪的茶的成功之道](https://mp.weixin.qq.com/s/a94n1k32ZwHRdrIlkwBxzw)
- [东南亚版的”蚂蚁金服”如何抢占蓝海市场？](https://mp.weixin.qq.com/s/452jfys0luQAAWQpA5kYhQ)

<div align="center">
    <img src="https://images.gitee.com/uploads/images/2020/0821/220601_c08feacc_1468963.png" width="600px">
</div>